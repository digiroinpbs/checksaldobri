package id.corechain

import com.google.gson.JsonObject
import com.google.gson.JsonParseException
import com.google.gson.JsonParser
import org.apache.commons.codec.digest.DigestUtils
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.TimeoutException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.Select
import org.openqa.selenium.support.ui.WebDriverWait
import spark.Spark.*
import java.util.*
import redis.clients.jedis.Jedis
import spark.ModelAndView
import spark.Request
import spark.Response
import spark.TemplateViewRoute
import spark.template.thymeleaf.ThymeleafTemplateEngine
import java.io.BufferedWriter
import java.io.FileWriter


class WebDriver(){
    fun getWebDriver(): WebDriver? {
        val props = Properties()
        props.load(this.javaClass.getResourceAsStream("/path.properties"))
        var driver: WebDriver
        System.setProperty("webdriver.chrome.driver", props.get("webdriver.chrome.driver").toString())
        driver = ChromeDriver()
        return driver
    }
}

fun getSaldo(req: Request, res: Response): ModelAndView {
    val jedis = Jedis("104.131.106.90",6379)
    jedis.auth("LCbpzEw2Zx5BNJaa63v9ivrSUIWzzjv9")
    jedis.select(0)
    var inquiry:String =jedis.get(req.params(":username"))
    val params = HashMap<String, Any>()
    val json: JsonObject
    val result:String
    val parser: JsonParser = JsonParser()
    try {
        json = parser.parse(inquiry) as JsonObject
        params.put("balance", json.get("amount").asString)
        params.put("bank",json.get("bank").asString)
        val timestamp:Long =json.get("lastupdate").asLong
        val time = java.util.Date(timestamp * 1000)
        params.put("update",time)
    }catch (e:Exception){
       e.printStackTrace()
    }
    params.put("account", req.params(":username"))


    return ModelAndView(params, "dashboard")
}

fun main(args: Array<String>) {
    port(7015)
    post("/checkSaldo") { req, res ->
        val json: JsonObject
        val result:String
        val parser: JsonParser = JsonParser()
        try {
            json = parser.parse(req.body()) as JsonObject
            result= checkSaldo(json.get("username").asString,json.get("password").asString)
        }catch (e:JsonParseException){
            e.printStackTrace()
            res.status(400)
            return@post "{invalid request}"
        }catch (e:TimeoutException){
            res.status(401)
            return@post "{time out}"
        }
        catch (e:Exception){
            e.printStackTrace()
            res.status(500)
            return@post "{system error}"
        }
        res.status(200)
        return@post result
    }
    get("/dashboard/:username", TemplateViewRoute { req, res -> getSaldo(req, res) }, ThymeleafTemplateEngine())
    post("/checkMutasi") { req, res ->

        val json: JsonObject
        val result:String
        val parser: JsonParser = JsonParser()
        try {
            json = parser.parse(req.body()) as JsonObject
            result= checkMutasi(json.get("username").asString,json.get("password").asString)
        }catch (e:Exception){
            e.printStackTrace()
            res.status(400)
            return@post "{invalid request}"
        }
        res.status(200)
        return@post result
    }
}

fun checkSaldo(username:String, password:String):String{
    val jedis = Jedis("104.131.106.90",6379)
    jedis.auth("LCbpzEw2Zx5BNJaa63v9ivrSUIWzzjv9")
    jedis.select(0)
    var result =""
    val webDriver =id.corechain.WebDriver().getWebDriver()
    val filename = "logCheckBalance.txt"
    if(webDriver!=null){
        webDriver.get("https://ib.bri.co.id/ib-bri/Login.html/en")
        var element = webDriver.findElement(By.name("j_captcha"))
        val wait = WebDriverWait(webDriver, 100000)
        wait.until(ExpectedConditions.attributeToBeNotEmpty(element,"value"));
        element = webDriver.findElement(By.name("j_plain_username"))
        element.sendKeys(username)
        element = webDriver.findElement(By.name("j_plain_password"))
        element.sendKeys(password)

        element.submit()
        webDriver.findElement(By.xpath("//a[@id = 'myaccounts']")).click()
        webDriver.switchTo().defaultContent()
        var frame = webDriver.findElement(By.name("menus"))
        webDriver.switchTo().frame(frame)
        webDriver.findElement(By.xpath("//div[@class = 'item']//a[@href='BalanceInquiry.html']")).click()
        webDriver.switchTo().defaultContent()
        frame = webDriver.findElement(By.name("content"))
        webDriver.switchTo().frame(frame)
        var table_element = webDriver.findElement(By.xpath("//table[@id = 'tabel-saldo']//tbody//tr//td[@style = 'text-align: right']"))
        var saldo =table_element.text
        saldo = saldo.replace(".","")
        result=result+"{\"bank\":\"BRI\",\"amount\":\""+saldo+"\",\"lastupdate\":\""+System.currentTimeMillis() / 1000L+"\"}"
        jedis.set(username+":BRI",result)
        webDriver.switchTo().defaultContent()
        webDriver.findElement(By.linkText("Logout")).click()

        var bw: BufferedWriter? = null
        var fw: FileWriter? = null
        try {
            var textResult = result.replace("{","")
            textResult = textResult.replace("}","")
            val content = textResult + "\n"
            fw = FileWriter(filename, true)
            bw = BufferedWriter(fw)
            bw.write(content)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                if (bw != null)
                    bw.close()
                if (fw != null)
                    fw.close()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }

        jedis.quit()
        webDriver.quit()
    }
    return result;
}

fun checkMutasi(username:String, password:String):String{
    val jedis = Jedis("104.131.106.90",6379)
    jedis.auth("LCbpzEw2Zx5BNJaa63v9ivrSUIWzzjv9")
    jedis.select(0)
    var result ="result :["
    val webDriver =id.corechain.WebDriver().getWebDriver()
    val filename = "logMutasiRekening.txt"
    var bw: BufferedWriter? = null
    var fw: FileWriter? = null
    if(webDriver!=null){
        webDriver.get("https://ib.bri.co.id/ib-bri/Login.html/en")
        var element = webDriver.findElement(By.name("j_captcha"))
        val wait = WebDriverWait(webDriver, 100000)
        wait.until(ExpectedConditions.attributeToBeNotEmpty(element,"value"));
        element = webDriver.findElement(By.name("j_plain_username"))
        element.sendKeys(username)
        element = webDriver.findElement(By.name("j_plain_password"))
        element.sendKeys(password)
        element.submit()

        webDriver.findElement(By.xpath("//a[@id = 'myaccounts']")).click()
        webDriver.switchTo().defaultContent()
        var frame = webDriver.findElement(By.name("menus"))
        webDriver.switchTo().frame(frame)
        webDriver.findElement(By.xpath("//div[@class = 'item']//a[@href='AccountStatement.html']")).click()

        webDriver.switchTo().defaultContent()
        frame = webDriver.findElement(By.name("content"))
        webDriver.switchTo().frame(frame)
        var select:Select = (Select(webDriver.findElement(By.xpath("//select[@name='ACCOUNT_NO']"))))
        select.selectByIndex(1)

        webDriver.findElement(By.ByXPath("//input[@name='submitButton']")).click()

        webDriver.switchTo().defaultContent()
        frame = webDriver.findElement(By.name("content"))
        webDriver.switchTo().frame(frame)
        val tr_collection = webDriver.findElements(By.xpath("//table[@class='box']//tbody//tr"))
        var row_num: Int
        var col_num: Int
        row_num = 1
        var first =true;
        var counter=0
        for (trElement in tr_collection) {
            val td_collection = trElement.findElements(By.xpath("td"))
            if(td_collection.size==0)break
            if(counter>10)break
            if(row_num<=1){
                row_num++
                counter++
                continue
            }else if(row_num>2 && row_num==tr_collection.size-2){
                break;
            } else if(!first){
                result=result+","
            }
            first=false
            col_num = 1
            var res=""
            for (tdElement in td_collection) {
                if(td_collection.size==1){
                    continue
                }
                if(col_num==1){
                    res=res+"{\"Tanggal\":\""+tdElement.text+"\","
                }else if(col_num==2){
                    res=res+"\"Keterangan Transaksi\":\""+tdElement.text+"\","
                }else if(col_num==3){
                    res=res+"\"Debet\":\""+tdElement.text+"\","
                }else if(col_num==4){
                    res=res+"\"Kredit\":\""+tdElement.text+"\","
                }else if(col_num==5){
                    res=res+"\"Saldo\":\""+tdElement.text+"\"}"
                }
                col_num++
            }

            if(jedis.get(username+":BRI:"+ sha1converter(res))==null && !res.equals("")){
                jedis.set(username+":BRI:"+ sha1converter(res),res)
                try {
                    var textResult = res.replace("{","")
                    textResult = textResult.replace("}","")
                    val content = textResult + "\n"
                    fw = FileWriter(filename, true)
                    bw = BufferedWriter(fw)
                    bw.write(content)
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    try {
                        if (bw != null)
                            bw.close()
                        if (fw != null)
                            fw.close()
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            }

            result=result+res;
            row_num++
            counter++
        }
        result =result+"]"
        webDriver.switchTo().defaultContent()
        webDriver.findElement(By.linkText("Logout")).click()

        jedis.quit()
        webDriver.quit()
    }
    return result;

}

fun sha1converter(param: String): String {
    return DigestUtils.sha1Hex(param)
}
